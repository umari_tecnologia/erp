package br.com.umari.dao.impl;

import java.io.Serializable;

import javax.ejb.Stateless;

import br.com.umari.dao.EstabelecimentoDAO;
import br.com.umari.entities.Estabelecimento;
@Stateless
public class EstabelecimentoDAOImpl extends GenericDAOImpl<Estabelecimento, Long>
		implements Serializable, EstabelecimentoDAO {

	private static final long serialVersionUID = -2599671247307980141L;
}
