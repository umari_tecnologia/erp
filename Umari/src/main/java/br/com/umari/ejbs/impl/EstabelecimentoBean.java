package br.com.umari.ejbs.impl;

import java.io.Serializable;
import java.util.Collection;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.umari.dao.EstabelecimentoDAO;
import br.com.umari.ejbs.EstabelecimentoEjb;
import br.com.umari.entities.Estabelecimento;

@Stateless
public class EstabelecimentoBean implements Serializable, EstabelecimentoEjb{

	private static final long serialVersionUID = -4871713213237905630L;

	@Inject
	private EstabelecimentoDAO dao;
	
	
	public void persist(Estabelecimento estabelecimento) {
		System.out.println("Entrou em EstabelecimentoBean");
		dao.persist(estabelecimento);
	}

	public void merge(Estabelecimento estabelecimento) {
		validarEstabelecimento(estabelecimento);
		dao.marge(estabelecimento);
	}

	public void remove(Estabelecimento estabelecimento) {
		if (estabelecimento.getId() == 0 || estabelecimento.getId() == null) {
			throw new IllegalArgumentException("Campo ID n�o pode ser nulo ou vazio");
		}
		dao.removeById(estabelecimento.getId());
	}

	public Collection<Estabelecimento> findAll() {
		return dao.findAll();
	}

	public Estabelecimento getById(Long id) {
		if (id == 0 || id == null) {
			throw new IllegalArgumentException("Campo ID n�o pode ser nulo ou vazio");
		}
		return dao.getByID(id);
	}
	
	private void validarEstabelecimento(Estabelecimento estabelecimento){
		
	}

}
