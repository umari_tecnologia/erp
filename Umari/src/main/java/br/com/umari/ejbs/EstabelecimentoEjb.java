package br.com.umari.ejbs;

import java.util.Collection;

import br.com.umari.entities.Estabelecimento;

public interface EstabelecimentoEjb {
	void persist(Estabelecimento estabelecimento);

	void merge(Estabelecimento estabelecimento);

	void remove(Estabelecimento estabelecimento);

	Collection<Estabelecimento> findAll();

	Estabelecimento getById(Long id);
}
