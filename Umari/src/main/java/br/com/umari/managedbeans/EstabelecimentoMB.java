package br.com.umari.managedbeans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

import org.primefaces.event.RowEditEvent;

import br.com.umari.ejbs.EstabelecimentoEjb;
import br.com.umari.entities.Estabelecimento;
import br.com.umari.util.UtilErros;
import br.com.umari.util.UtilMensagens;


@RequestScoped
@ManagedBean(name="estabelecimentoMB")
public class EstabelecimentoMB implements Serializable{
	private static final long serialVersionUID = 2941386880795673862L;
	
	@Inject
	private EstabelecimentoEjb bean;
	
	private Estabelecimento estabelecimento;
	
	private List<Estabelecimento> estabelecimentos;
	
	@PostConstruct
	public void init(){
		estabelecimentos = (List<Estabelecimento>) bean.findAll();
		estabelecimento = new Estabelecimento();
	}
	
	public String cadastrar(){
		try {
			bean.persist(estabelecimento);
			estabelecimentos = (List<Estabelecimento>) bean.findAll();
		} catch (Exception e) {

		}
		
		return "";
	}
	
	public String atualizar(){
		try{
			bean.merge(estabelecimento);
		}catch(Exception e){
			UtilMensagens.mensagemErro(UtilErros.getMensagemErro(e));
		}
		
		return "";
	}
	
	public void excluir(){
		try {
			bean.remove(estabelecimento);
			estabelecimentos = (List<Estabelecimento>) bean.findAll();
		} catch (Exception e) {
			UtilMensagens.mensagemErro(UtilErros.getMensagemErro(e));
		}
	}
	
	public void onEdit(RowEditEvent event){
		estabelecimento = (Estabelecimento) event.getObject();
		atualizar();
		UtilMensagens.mensagemInformacao("Estabelecimento " + estabelecimento.getRazaoSocial() + " atualizado!");
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public List<Estabelecimento> getEstabelecimentos() {
		return estabelecimentos;
	}

	public void setEstabelecimentos(List<Estabelecimento> estabelecimentos) {
		this.estabelecimentos = estabelecimentos;
	}
	
	
	

}
