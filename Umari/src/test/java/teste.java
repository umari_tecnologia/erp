import javax.persistence.EntityManager;

import br.com.umari.dao.impl.EntityManagerUtil;
import br.com.umari.dao.impl.GenericDAOImpl;
import br.com.umari.ejbs.impl.EstabelecimentoBean;
import br.com.umari.entities.Estabelecimento;

public class teste {

	public static void main(String[] args) {
		//GenericDAOImpl<Estabelecimento, Long> e = new GenericDAOImpl<Estabelecimento, Long>();
		/*
		List<Estabelecimento> list;
		list = (List<Estabelecimento>)e.findAll();
		
		for (Estabelecimento estabelecimento : list) {
			System.out.println(estabelecimento.getCNPJ());
		}
*/
		EntityManager em = EntityManagerUtil.getEntityManager();
		
		Estabelecimento estabelecimento = new Estabelecimento();
		
		Long l = new Long(1);
		
		estabelecimento.setId(l);
		estabelecimento.setCNPJ("123");
		estabelecimento.setEmail("c@c.c");
		estabelecimento.setIE("1");
		estabelecimento.setIM("2");
		estabelecimento.setNomeFantasia("Estabelecimento fantasia");
		estabelecimento.setRazaoSocial("Estabelecimento razao social");
		estabelecimento.setTelefone("12343");
		
		em.getTransaction().begin();
		em.persist(estabelecimento);
		em.getTransaction().commit();
		//e.persist(estabelecimento);
	}

}
